package com.kavak.poc.spring.controllers;

import com.kavak.poc.spring.user.adapters.outbound.rest.dto.UserDto;
import com.kavak.poc.spring.user.adapters.outbound.rest.repository.UserAPIRepository;
import com.kavak.poc.spring.user.domain.model.User;
import datadog.trace.api.GlobalTracer;
import datadog.trace.api.Trace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(path = "/")
public class DemoController {

  @Autowired
  private UserAPIRepository userAPIRepository;

  @GetMapping("/{id}")
  public Mono<ResponseEntity<UserDto>> index(@PathVariable("id") String id) {
    return userAPIRepository.get(id)
        .map(userDto -> ResponseEntity.ok(userDto)).defaultIfEmpty(ResponseEntity.notFound().build());
  }

  @PostMapping
  public Mono<UserDto> create(@RequestBody User body) {
    return userAPIRepository.create(body);
  }
}
