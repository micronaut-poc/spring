package com.kavak.poc.spring._shared.api.filters.log;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonRawValue;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import java.io.Serializable;
import java.net.URL;
import java.util.Collection;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class RestCommunicationLog implements Serializable {

  @NotBlank
  private String remoteHost;
  @NotBlank private String method;
  @NotNull
  private URL url;
  @NotNull private Map<String, Collection<String>> requestHeaders;
  @JsonRawValue private String requestPayload;
  private int responseStatus;
  @NotNull private Map<String, Collection<String>> responseHeaders;

  @JsonInclude(Include.NON_NULL)
  @JsonRawValue
  private String responsePayload;

  @Positive
  private long elapsedTime;
}
