package com.kavak.poc.spring._shared.api;

import java.time.Duration;
import java.util.List;
import lombok.experimental.UtilityClass;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import reactor.netty.resources.ConnectionProvider;

@UtilityClass
public class WebClientStaticFactory {

  public static WebClient createCustomWebClient(
      List<ExchangeFilterFunction> filters) {

    WebClient.Builder webClientBuilder = WebClient.builder();

    for (ExchangeFilterFunction filter : filters) {
      webClientBuilder.filter(filter);
    }

    webClientBuilder.baseUrl("http://localhost:9001");
    webClientBuilder.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
    webClientBuilder.clientConnector(clientConnector());

    return webClientBuilder.build();
  }

  ClientHttpConnector clientConnector() {
    return new ReactorClientHttpConnector(
        HttpClient.create(
            ConnectionProvider.builder("fixed")
                .maxConnections(1000)
                .pendingAcquireMaxCount(1000)
                .lifo()
                .evictInBackground(Duration.ofSeconds(60))
                .build()));
  }
}
