package com.kavak.poc.spring._shared.api.filters.log;

import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "logging-filter")
@Getter
@Setter
public class LoggingFilterProperties {

  private Set<String> excludeUrls;
  private Set<String> excludeHeaders;
}
