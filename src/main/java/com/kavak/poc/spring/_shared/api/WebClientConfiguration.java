package com.kavak.poc.spring._shared.api;

import java.util.List;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@RequiredArgsConstructor
@Getter
@Slf4j
public class WebClientConfiguration {


  @Bean(name = "webClient")
  public WebClient createWebClient() {
    return WebClientStaticFactory.createCustomWebClient(
        List.of(
            WebClientFilters.whenResponseError("demo-api")));
  }
}
