package com.kavak.poc.spring._shared.api;

import lombok.experimental.UtilityClass;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import reactor.core.publisher.Mono;

@UtilityClass
public class WebClientFilters {


  public static ExchangeFilterFunction whenResponseError(String workload) {
    return ExchangeFilterFunction.ofResponseProcessor(
        (clientResponse) -> {
          if (clientResponse.statusCode().isError()) {
            return clientResponse
                .bodyToMono(String.class)
                .flatMap(
                    body ->
                        Mono.error(
                            new ApiClientException(
                                workload, body, (HttpStatus) clientResponse.statusCode())));
          }
          return Mono.just(clientResponse);
        });
  }
}
