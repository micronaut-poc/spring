package com.kavak.catalog.sku.api._shared.api.advice;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@Order(0)
@Slf4j
public class ControllerAdvisor {

  private static final String ERROR_TITLE = "ERROR_TITLE";
  private static final String ERROR_DETAIL = "ERROR_DETAIL";

  @ExceptionHandler({Throwable.class})
  public ResponseEntity<ProblemDetail> handleAll(
      Throwable ex, WebRequest request) {
    ProblemDetail problemDetail = ProblemDetail.forStatus(INTERNAL_SERVER_ERROR);
    problemDetail.setDetail(ex.getMessage());
    return new ResponseEntity<>(problemDetail, INTERNAL_SERVER_ERROR);
  }
}
