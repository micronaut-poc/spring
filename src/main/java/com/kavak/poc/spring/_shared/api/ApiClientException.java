package com.kavak.poc.spring._shared.api;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@RequiredArgsConstructor
@Getter
public class ApiClientException extends RuntimeException {
  private final String service;
  private final String message;
  private final HttpStatus httpStatus;
}
