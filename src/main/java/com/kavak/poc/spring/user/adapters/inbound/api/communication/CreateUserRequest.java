package com.kavak.poc.spring.user.adapters.inbound.api.communication;

import jakarta.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CreateUserRequest implements Serializable {

  @NotBlank private String name;
  private LocalDate bornDate;
}
