package com.kavak.poc.spring.user.adapters.outbound.rest.repository;

import com.kavak.poc.spring.user.adapters.outbound.rest.dto.UserDto;
import com.kavak.poc.spring.user.domain.model.User;
import com.kavak.poc.spring.user.domain.ports.UserAPIPort;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class UserAPIRepository implements UserAPIPort {

  private WebClient httpClient;

  public UserAPIRepository(WebClient httpClient) {
    this.httpClient = httpClient;
  }

  public UserDto getBlocking(String userId) {
    return httpClient
        .method(HttpMethod.GET)
        .uri("/%s".formatted(userId))
        .retrieve()
        .bodyToMono(UserDto.class)
        .block();
  }

  public Mono<UserDto> get(String userId) {
    return httpClient
        .method(HttpMethod.GET)
        .uri("/%s".formatted(userId))
        .retrieve()
        .bodyToMono(UserDto.class);
  }

  public Mono<UserDto> create(User user) {
    return httpClient
        .method(HttpMethod.POST)
        .uri("/")
        .bodyValue(user)
        .retrieve()
        .bodyToMono(UserDto.class);
  }
}
