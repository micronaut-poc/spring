package com.kavak.poc.spring.user.adapters.inbound.api.controller;

import com.kavak.poc.spring.user.adapters.inbound.api.communication.CreateUserRequest;
import com.kavak.poc.spring.user.domain.actions.create.UserCreateUseCase;
import com.kavak.poc.spring.user.domain.actions.find.UserFindUseCase;
import com.kavak.poc.spring.user.domain.model.User;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@AllArgsConstructor
public class UserController {

  private final UserFindUseCase userFindUseCase;
  private final UserCreateUseCase userCreateUseCase;

  @GetMapping(value = "/users/{id}")
  public User get(@PathVariable UUID id) {
    return userFindUseCase.findById(id);
  }
  @PostMapping(value = "/users")
  public User create(@RequestBody CreateUserRequest body) {
    return userCreateUseCase.create(
        User.builder()
            .id(UUID.randomUUID())
            .bornDate(body.getBornDate())
            .name(body.getName())
            .build());
  }
}
