package com.kavak.poc.spring.user.adapters.outbound.db.repository;

import com.kavak.poc.spring.user.adapters.outbound.db.entity.UserEntity;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaUserRepository extends JpaRepository<UserEntity, UUID> {}
