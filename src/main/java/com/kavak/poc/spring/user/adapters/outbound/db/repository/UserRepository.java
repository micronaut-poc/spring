package com.kavak.poc.spring.user.adapters.outbound.db.repository;

import com.kavak.poc.spring.user.adapters.outbound.db.entity.UserEntity;
import com.kavak.poc.spring.user.domain.model.User;
import com.kavak.poc.spring.user.domain.ports.UserRepositoryPort;
import java.util.Optional;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@AllArgsConstructor
public class UserRepository implements UserRepositoryPort {
  private com.kavak.poc.spring.user.adapters.outbound.db.repository.JpaUserRepository repository;

  @Override
  public Optional<User> findById(UUID id) {
    Optional<UserEntity> user = repository.findById(id);
    return user.map(
        userEntity ->
            User.builder()
                .id(userEntity.getId())
                .bornDate(userEntity.getBornDate())
                .name(userEntity.getName())
                .build());
  }

  @Override
  public User save(User user) {
    UserEntity userEntity =
        UserEntity.builder()
            .id(user.getId())
            .bornDate(user.getBornDate())
            .name(user.getName())
            .build();
    repository.save(userEntity);
    return user;
  }
}
