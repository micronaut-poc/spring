package com.kavak.poc.spring.user.domain.actions.find;

import com.kavak.poc.spring.user.domain.model.User;
import com.kavak.poc.spring.user.domain.ports.UserRepositoryPort;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserFindUserCaseImpl implements
    com.kavak.poc.spring.user.domain.actions.find.UserFindUseCase {

  private UserRepositoryPort repository;

  @Override
  @Cacheable(cacheNames = "users", key = "#id")
  public User findById(UUID id) {
    return repository.findById(id).orElseThrow();
  }
}
