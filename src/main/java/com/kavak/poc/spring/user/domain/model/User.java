package com.kavak.poc.spring.user.domain.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class User implements Serializable {

  private UUID id;
  private String name;
  private LocalDate bornDate;
}
