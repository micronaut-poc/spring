package com.kavak.poc.spring.user.domain.actions.create;

import com.kavak.poc.spring.user.domain.model.User;
import com.kavak.poc.spring.user.domain.ports.UserRepositoryPort;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserCreateUseCaseImpl implements UserCreateUseCase {

  private UserRepositoryPort repository;

  @Override
  public User create(User user) {
    return repository.save(user);
  }
}
