package com.kavak.poc.spring.user.domain.actions.find;

import com.kavak.poc.spring.user.domain.model.User;
import java.util.UUID;

public interface UserFindUseCase {
  User findById(UUID id);
}
