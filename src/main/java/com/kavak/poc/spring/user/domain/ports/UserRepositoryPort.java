package com.kavak.poc.spring.user.domain.ports;

import com.kavak.poc.spring.user.domain.model.User;
import java.util.Optional;
import java.util.UUID;

public interface UserRepositoryPort {

  Optional<User> findById(UUID id);
  User save(User user);
}
