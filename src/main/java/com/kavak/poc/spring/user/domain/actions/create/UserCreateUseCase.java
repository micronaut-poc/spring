package com.kavak.poc.spring.user.domain.actions.create;

import com.kavak.poc.spring.user.domain.model.User;

public interface UserCreateUseCase {

  User create(User userDto);
}
